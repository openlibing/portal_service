package com.huawei.ci.portal;

import com.huawei.ci.common.kms.KmsUtils;
import org.apache.servicecomb.springboot2.starter.EnableServiceComb;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
@EnableServiceComb
public class CiPortalServiceApplication {

    public static void main(String[] args) {
        try {
            new SpringApplicationBuilder().web(WebApplicationType.NONE).sources(CiPortalServiceApplication.class).run(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
