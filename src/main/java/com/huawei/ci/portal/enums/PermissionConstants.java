package com.huawei.ci.portal.enums;

public class PermissionConstants {
    public static String ID = "id";
    public static String TOKEN = "token";
    public static String NAME = "name";
    public static String AVATAR_URL = "avatar_url";
    public static String EMAIL = "email";
    public static String USER_ID = "userId";
    public static String USER_NAME = "userName";
}
