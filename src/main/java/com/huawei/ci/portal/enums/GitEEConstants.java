package com.huawei.ci.portal.enums;

public class GitEEConstants {
    public static String ERROR_MESSAGE = "error_message";

    public static String CONTENT_LENGTH = "Content-Length";
    public static String CONTENT_TYPE = "Content-Type";
    public static String CONTENT_TYPE_VALUE = "application/json;charset=UTF-8";

    public static String ERROR = "error";
    public static String SUCCESS = "success";

    public static String ACCESS_TOKEN = "access_token";

}
