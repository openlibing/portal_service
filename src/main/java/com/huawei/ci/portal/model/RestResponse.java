package com.huawei.ci.portal.model;

import lombok.Data;

/**
 * 响应参数
 */
@Data
public class RestResponse<T> {
    private Integer code;
    private T data;
    private String message;

    public RestResponse() {
    }

    public RestResponse(Integer code, T date, String message) {
        this.code = code;
        this.data = date;
        this.message = message;
    }
}
