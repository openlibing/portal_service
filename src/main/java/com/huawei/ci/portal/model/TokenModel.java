package com.huawei.ci.portal.model;

import lombok.Data;

@Data
public class TokenModel {
    Long timestamp;
    String token;

    public TokenModel(){

    }


    public TokenModel(Long timestamp, String token) {
        this.timestamp = timestamp;
        this.token = token;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
