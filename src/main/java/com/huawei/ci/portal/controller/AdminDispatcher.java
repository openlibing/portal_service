package com.huawei.ci.portal.controller;


import com.huawei.ci.portal.model.TokenModel;
import com.huawei.ci.portal.util.AuthHelper;
import com.huawei.ci.portal.util.HttpContextUtil;
import com.netflix.config.DynamicPropertyFactory;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.RequestOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang.StringUtils;
import org.apache.servicecomb.core.Transport;
import org.apache.servicecomb.edge.core.AbstractEdgeDispatcher;
import org.apache.servicecomb.foundation.common.net.URIEndpointObject;
import org.apache.servicecomb.foundation.vertx.client.http.HttpClients;
import org.apache.servicecomb.loadbalance.ServiceCombServer;
import org.apache.servicecomb.registry.cache.CacheEndpoint;
import org.apache.servicecomb.transport.rest.servlet.ServletRestTransport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class AdminDispatcher extends AbstractEdgeDispatcher {

    private static final Logger LOG = LoggerFactory.getLogger(AdminDispatcher.class);

    private static final String GATEWAY_URL=DynamicPropertyFactory.getInstance().getStringProperty("servicecomb.http.dispatcher.edge.default.gateway","").get();

    @Override
    public int getOrder() {
        return 10003;
    }

    @Override
    public void init(Router router) {
        String pattern = DynamicPropertyFactory.getInstance().getStringProperty("servicecomb.http.dispatcher.edge.http.adminPattern", "").get();
        if(pattern.equals("")){
            throw new RuntimeException("url pattern is empty");
        }
        router.routeWithRegex(pattern).failureHandler(this::onFailure).handler(this::onRequest);
    }
    protected void onRequest(RoutingContext context) {
        String uri = context.request().uri();
        ServiceCombServer server = getCombService();
        if (server == null) {
            context.response().setStatusCode(503);
            context.response().setStatusMessage("service not ready");
            context.response().end();
        } else {
            URIEndpointObject endpointObject = new URIEndpointObject(server.getEndpoint().getEndpoint());
            LOG.info("Gateway total url is "+server.getId()+uri);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.setHost(endpointObject.getHostOrIp()).setPort(endpointObject.getPort()).setSsl(endpointObject.isSslEnabled()).setURI(uri);
            HttpClient httpClient;
            if (endpointObject.isHttp2Enabled()) {
                httpClient = HttpClients.getClient("http2-transport-client", false).getHttpClient();
            } else {
                httpClient = HttpClients.getClient("http-transport-client", false).getHttpClient();
            }

            HttpClientRequest httpClientRequest = httpClient.request(context.request().method(),null, requestOptions, (httpClientResponse) -> {
                context.response().setStatusCode(httpClientResponse.statusCode());
                httpClientResponse.headers().forEach((header) -> {
                    context.response().headers().set(header.getKey(), header.getValue());
                });
                httpClientResponse.handler(this.responseHandler(context, httpClientResponse));
                httpClientResponse.endHandler((v) -> {
                    context.response().end();
                });
            });
            //追加请求头
            TokenModel tokenModel = AuthHelper.getInstance().createToken();
            context.request().headers().add("ci-token", tokenModel.getToken());
            context.request().headers().add("timestamp", "" + tokenModel.getTimestamp());
            context.request().headers().forEach((header) -> {
                httpClientRequest.headers().set((String) header.getKey(), (String) header.getValue());
            });
            context.request().handler((data) -> {
                httpClientRequest.write(data);
            });
            context.request().endHandler((v) -> {
                httpClientRequest.end();
            });
        }

    }

    private ServiceCombServer getCombService() {
        String microserviceName = "gateway";
        Transport transport = new ServletRestTransport();
        CacheEndpoint cacheEndpoint = new CacheEndpoint(GATEWAY_URL, null);
        LOG.info("Gateway combService url is "+GATEWAY_URL);
        return new ServiceCombServer(microserviceName, transport, cacheEndpoint);
    }

    protected Handler<Buffer> responseHandler(RoutingContext routingContext, HttpClientResponse httpClientResponse) {
        return (data) -> {
            routingContext.response().write(data);
        };
    }

    /**
     * 如果匹配失败了
     */
    @Override
    protected void onFailure(RoutingContext context) {
        HttpContextUtil.response(context, 404);
    }

    private boolean getUserContext(RoutingContext context) {
        String ciToken = context.request().getHeader("ci-token");
        String timestamp = context.request().getHeader("timestamp");
        if (StringUtils.isBlank(ciToken) || StringUtils.isBlank(timestamp)) {
            return false;
        }
        try {
            return AuthHelper.getInstance().initUserContext(ciToken, timestamp);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException | InvalidKeyException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }
        return false;
    }

}
