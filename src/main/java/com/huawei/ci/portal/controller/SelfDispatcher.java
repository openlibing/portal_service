package com.huawei.ci.portal.controller;

import com.huawei.ci.common.utils.JsonUtils;
import com.huawei.ci.portal.enums.GitEEConstants;
import com.huawei.ci.portal.enums.PermissionConstants;
import com.huawei.ci.portal.model.InfoModel;
import com.huawei.ci.portal.util.GiteeOAuthUtil;
import com.huawei.ci.portal.util.HttpContextUtil;
import com.huawei.ci.portal.util.JwtUtils;
import com.netflix.config.DynamicPropertyFactory;
import io.vertx.core.http.impl.CookieImpl;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang.StringUtils;
import org.apache.servicecomb.core.Transport;
import org.apache.servicecomb.edge.core.AbstractEdgeDispatcher;
import org.apache.servicecomb.loadbalance.ServiceCombServer;
import org.apache.servicecomb.registry.cache.CacheEndpoint;
import org.apache.servicecomb.transport.rest.servlet.ServletRestTransport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class SelfDispatcher extends AbstractEdgeDispatcher {
    private static final Logger LOG = LoggerFactory.getLogger(SelfDispatcher.class);

    private final String GATEWAY_URL = DynamicPropertyFactory.getInstance().getStringProperty("servicecomb.http.dispatcher.edge.default.gateway", "").get();

    private final String APP_PAGE = DynamicPropertyFactory.getInstance().getStringProperty("gitee.oauth.app.page", "").get();

    private ServiceCombServer server = null;

    private GiteeOAuthUtil giteeOAuthUtil = GiteeOAuthUtil.getInstance();

    @Override
    public int getOrder() {
        return 10004;
    }

    @Override
    public void init(Router router) {
        server = getCombService();
        String pattern = DynamicPropertyFactory.getInstance().getStringProperty("servicecomb.http.dispatcher.edge.http.selfPattern", "").get();
        if (pattern.equals("")) {
            throw new RuntimeException("url pattern is empty");
        }
        router.routeWithRegex(pattern).failureHandler(this::onFailure).handler(this::onRequest);
    }

    protected void onRequest(RoutingContext context) {
        String path = context.request().path();

        if (path.equals("/api/portal-service/login")) {
            login(context);
            return;
        }
        if (path.equals("/api/portal-service/validateLogin")) {
            callBack(context);
            return;
        }
        // 从 cookie中获取token
        String tokenFromCookie = JwtUtils.getTokenFromCookie(context);
        if (StringUtils.isBlank(tokenFromCookie)) {
            return;
        }
        // 校验token的合法性
        boolean verifyToken = JwtUtils.verifyToken(tokenFromCookie, context);
        if (!verifyToken) {
            return;
        }

        if (path.equals("/api/portal-service/getInfo")) {
            getInfo(context, tokenFromCookie);
            return;
        }
        if (path.equals("/api/portal-service/logout")) {
            logout(context);
            return;
        }


    }

    @Override
    protected void onFailure(RoutingContext context) {
        HttpContextUtil.response(context, 404);
    }

    private ServiceCombServer getCombService() {
        String microserviceName = "gateway";
        Transport transport = new ServletRestTransport();
        CacheEndpoint cacheEndpoint = new CacheEndpoint(GATEWAY_URL, null);
        LOG.info("Gateway combService url is " + GATEWAY_URL);
        return new ServiceCombServer(microserviceName, transport, cacheEndpoint);
    }

    private void login(RoutingContext context) {
        context.response().setStatusCode(200);
        context.response().setStatusMessage(GitEEConstants.SUCCESS);
        HashMap<String, String> map = new HashMap<>();
        map.put("loginUrl", giteeOAuthUtil.getLoginUrl());
        String resJson = JsonUtils.toJson(map);
        context.response().headers().add(GitEEConstants.CONTENT_LENGTH, String.valueOf(resJson.length()));
        context.response().headers().add(GitEEConstants.CONTENT_TYPE, GitEEConstants.CONTENT_TYPE_VALUE);

        context.response().write(resJson).close();

        context.response().end();
    }

    private void callBack(RoutingContext context) {
        String code = context.request().params().get("code");
        HashMap<String, String> token = giteeOAuthUtil.getToken(code);
        HashMap<String, String> res = new HashMap<>();
        context.response().headers().add(GitEEConstants.CONTENT_TYPE, GitEEConstants.CONTENT_TYPE_VALUE);
        String accessToken = token.get(GitEEConstants.ACCESS_TOKEN);
        if (StringUtils.isNotEmpty(accessToken)) {
            HashMap<String, String> info = GiteeOAuthUtil.getInfo(accessToken);
            if (StringUtils.isNotEmpty(info.get(PermissionConstants.NAME))) {

                InfoModel infoModel = new InfoModel();
                infoModel.setId(info.get(PermissionConstants.ID));
                infoModel.setAccessToken(accessToken);
                infoModel.setName(info.get(PermissionConstants.NAME));
                infoModel.setAvatarUrl(info.get(PermissionConstants.AVATAR_URL));
                infoModel.setEmail(info.get("email"));
                String oauthToken = JwtUtils.getToken(infoModel);
                context.response().setStatusCode(302);
                context.response().setStatusMessage(GitEEConstants.SUCCESS);
                CookieImpl cookie = getCookie(oauthToken, 12 * 60 * 60);
                context.response().addCookie(cookie);
                context.response().headers().add("location", APP_PAGE);
                context.response().end();
            } else {
                LOG.error("get info error  result is {}", JsonUtils.toJson(info));
                String errorMessage = info.get(GitEEConstants.ERROR_MESSAGE);
                res.put(GitEEConstants.ERROR_MESSAGE, StringUtils.isEmpty(errorMessage) ? "accessToken wrong or expired" : errorMessage);
                HttpContextUtil.result(context, res, 500);
            }
        } else {
            LOG.error("get token error  result is {}", JsonUtils.toJson(token));
            String errorMessage = token.get(GitEEConstants.ERROR_MESSAGE);
            res.put(GitEEConstants.ERROR_MESSAGE, StringUtils.isEmpty(errorMessage) ? "code wrong or expired" : errorMessage);
            HttpContextUtil.result(context, res, 500);
        }

    }

    private void getInfo(RoutingContext context, String token) {

        String userId = JwtUtils.getClaimByName(token, PermissionConstants.ID).as(String.class);
        String avatarUrl = JwtUtils.getClaimByName(token, PermissionConstants.AVATAR_URL).as(String.class);

        HashMap<String, String> result = new HashMap<>();
        result.put(PermissionConstants.USER_ID, userId);
        result.put(PermissionConstants.AVATAR_URL, avatarUrl);
        HttpContextUtil.result(context, result,200);
    }

    private void logout(RoutingContext context) {
//        context.response().removeCookie(PermissionConstants.TOKEN);
        CookieImpl cookie = getCookie("", 0);
        context.response().addCookie(cookie);
        HashMap<String, String> map = new HashMap<>();
        map.put("result", "success");
        HttpContextUtil.result(context, map, 200);

    }

    private CookieImpl getCookie(String value, int age){
        CookieImpl cookie = new CookieImpl("token", value);
        cookie.setHttpOnly(true);
        cookie.setMaxAge(age);
        cookie.setPath("/api/");
        return cookie;
    }


}
