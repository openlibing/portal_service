package com.huawei.ci.portal.controller;


import com.huawei.ci.common.utils.JsonUtils;
import com.huawei.ci.portal.enums.GitEEConstants;
import com.huawei.ci.portal.enums.PermissionConstants;
import com.huawei.ci.portal.model.TokenModel;
import com.huawei.ci.portal.util.AuthHelper;
import com.huawei.ci.portal.util.HttpContextUtil;
import com.huawei.ci.portal.util.JwtUtils;
import com.netflix.config.DynamicPropertyFactory;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.*;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang.StringUtils;
import org.apache.servicecomb.core.Transport;
import org.apache.servicecomb.edge.core.*;
import org.apache.servicecomb.foundation.common.net.URIEndpointObject;
import org.apache.servicecomb.foundation.vertx.client.http.HttpClients;
import org.apache.servicecomb.loadbalance.ServiceCombServer;
import org.apache.servicecomb.registry.cache.CacheEndpoint;
import org.apache.servicecomb.transport.rest.servlet.ServletRestTransport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Objects;

public class DefaultDispatcher extends AbstractEdgeDispatcher {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultDispatcher.class);

    private static final String GATEWAY_URL = DynamicPropertyFactory.getInstance().getStringProperty("servicecomb.http.dispatcher.edge.default.gateway", "").get();

    private ServiceCombServer server = null;

    @Override
    public int getOrder() {
        return 10002;
    }

    @Override
    public void init(Router router) {
        server = getCombService();
        String pattern = DynamicPropertyFactory.getInstance().getStringProperty("servicecomb.http.dispatcher.edge.http.defaultPattern", "").get();
        if (pattern.equals("")) {
            throw new RuntimeException("url pattern is empty");
        }
        router.routeWithRegex(pattern).failureHandler(this::onFailure).handler(this::onRequest);
    }

    protected void onRequest(RoutingContext context) {
        String uri = context.request().uri();

        if (server == null) {
            context.response().setStatusCode(503);
            context.response().setStatusMessage("service not ready");
            context.response().end();
        } else {
            // 从 cookie中获取token
            Cookie tokenCookie = context.request().getCookie(PermissionConstants.TOKEN);
            String tokenFromCookie = Objects.isNull(tokenCookie) ? "" : tokenCookie.getValue();
            if (StringUtils.isNotBlank(tokenFromCookie)) {
                // 校验token的合法性
                boolean verifyToken = JwtUtils.verifyToken(tokenFromCookie, context);
                if (!verifyToken) {
                    return;
                }
                String userId = JwtUtils.getClaimByName(tokenFromCookie, PermissionConstants.ID).as(String.class);
                String name = JwtUtils.getClaimByName(tokenFromCookie, PermissionConstants.NAME).as(String.class);
                String query = context.request().query();
                String separator = StringUtils.isBlank(query) ? "?" : "&";
                uri = String.format("%s%s%s=%s&%s=%s", uri, separator, PermissionConstants.USER_ID, userId, PermissionConstants.USER_NAME, name);
            } else {
                String query = context.request().query();
                if (StringUtils.isNotBlank(query) && query.contains(PermissionConstants.USER_ID)) {
                    String value = context.request().getParam(PermissionConstants.USER_ID);
                    uri = uri.replaceAll(String.format("%s=%s", PermissionConstants.USER_ID, value), String.format("%s=", PermissionConstants.USER_ID));
                }
                if (StringUtils.isNotBlank(query) && query.contains(PermissionConstants.USER_NAME)) {
                    String value = context.request().getParam(PermissionConstants.USER_NAME);
                    uri = uri.replaceAll(String.format("%s=%s", PermissionConstants.USER_NAME, value), String.format("%s=", PermissionConstants.USER_NAME));
                }

            }

            URIEndpointObject endpointObject = new URIEndpointObject(server.getEndpoint().getEndpoint());
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.setHost(endpointObject.getHostOrIp()).setPort(endpointObject.getPort()).setSsl(endpointObject.isSslEnabled()).setURI(uri);
            HttpClient httpClient;
            if (endpointObject.isHttp2Enabled()) {
                httpClient = HttpClients.getClient("http2-transport-client", false).getHttpClient();
            } else {
                httpClient = HttpClients.getClient("http-transport-client", false).getHttpClient();
            }

            HttpClientRequest httpClientRequest = httpClient.request(context.request().method(), null, requestOptions, (httpClientResponse) -> {
                context.response().setStatusCode(httpClientResponse.statusCode());
                httpClientResponse.headers().forEach((header) -> {
                    context.response().headers().set(header.getKey(), header.getValue());
                });
                httpClientResponse.handler(this.responseHandler(context, httpClientResponse));
                httpClientResponse.endHandler((v) -> {
                    context.response().end();
                });
            });
//            追加请求头
//            TokenModel tokenModel = AuthHelper.getInstance().createToken();
            AuthHelper instance = AuthHelper.getInstance();
            TokenModel tokenModel = instance.createToken();
            context.request().headers().add("ci-token", tokenModel.getToken());


            context.request().headers().add("timestamp", ""+ tokenModel.getTimestamp() );
            context.request().headers().forEach((header) -> {
                httpClientRequest.headers().set((String) header.getKey(), (String) header.getValue());
            });
            context.request().handler((data) -> {
                httpClientRequest.write(data);
            });
            context.request().endHandler((v) -> {
                httpClientRequest.end();
            });
        }

    }


    private ServiceCombServer getCombService() {
        String microserviceName = "gateway";
        Transport transport = new ServletRestTransport();
        CacheEndpoint cacheEndpoint = new CacheEndpoint(GATEWAY_URL, null);
        LOG.info("Gateway combService url is " + GATEWAY_URL);
        return new ServiceCombServer(microserviceName, transport, cacheEndpoint);
    }

    protected Handler<Buffer> responseHandler(RoutingContext routingContext, HttpClientResponse httpClientResponse) {
        return (data) -> {
            routingContext.response().write(data);
        };
    }

    /**
     * 如果匹配失败了
     */
    @Override
    protected void onFailure(RoutingContext context) {
        HttpContextUtil.response(context, 404);
    }

    private boolean getUserContext(RoutingContext context) {
        String ciToken = context.request().getHeader("ci-token");
        String timestamp = context.request().getHeader("timestamp");
        if (StringUtils.isBlank(ciToken) || StringUtils.isBlank(timestamp)) {
            return false;
        }
        try {
            return AuthHelper.getInstance().initUserContext(ciToken, timestamp);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException | InvalidKeyException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }
        return false;
    }

    private void failed(RoutingContext context, HashMap<String, String> res) {
        context.response().setStatusCode(500);
        context.response().setStatusMessage(GitEEConstants.ERROR);
        String resJson = JsonUtils.toJson(res);
        context.response().headers().add(GitEEConstants.CONTENT_LENGTH, String.valueOf(resJson.length()));
        context.response().write(resJson).close();
    }
}
