package com.huawei.ci.portal.filter;

import org.apache.servicecomb.core.Handler;
import org.apache.servicecomb.core.Invocation;
import org.apache.servicecomb.swagger.invocation.AsyncResponse;


public class AuthHandler implements Handler {

    @Override
    public void handle(Invocation invocation, AsyncResponse asyncResponse) throws Exception {
        invocation.next(asyncResponse);
    }
}
