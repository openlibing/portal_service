package com.huawei.ci.portal.util;

import com.huawei.ci.common.kms.KmsUtils;
import com.huawei.ci.portal.model.TokenModel;
import com.netflix.config.DynamicPropertyFactory;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class AuthHelper {

    private static final String SECRET = DynamicPropertyFactory.getInstance().getStringProperty("portalService.secret","").get();

    private static final AuthHelper AUTH_HELPER = new AuthHelper();

    private String decryptSecret ="123";
    private AuthHelper() {
//        decryptSecret = KmsUtils.decrypt(SECRET);
        decryptSecret = "123";
    }

    public static AuthHelper getInstance() {
        return AUTH_HELPER;
    }

    /**
     * 根据用户创建token
     */
    public TokenModel createToken(){
        Long timestamp = System.currentTimeMillis();
        //拼接时间戳和用户信息字符串
        String stringToSign = timestamp + " " + decryptSecret;
        Mac mac;
        try {
            mac = Mac.getInstance("HmacSHA256");mac.init(new SecretKeySpec(decryptSecret.getBytes("UTF-8"), "HmacSHA256"));
            byte[] signData = mac.doFinal(stringToSign.getBytes("UTF-8"));
            String userToken = URLEncoder.encode(new String(Base64.encodeBase64(signData)), "UTF-8");
            return new TokenModel(timestamp, userToken);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据token获取用户信息
     */
    public boolean initUserContext(String token, String timestamp) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {
        String stringToSign = timestamp + " " + decryptSecret;
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(new SecretKeySpec(decryptSecret.getBytes("UTF-8"), "HmacSHA256"));
        byte[] signData = mac.doFinal(stringToSign.getBytes("UTF-8"));
        String userToken = URLEncoder.encode(new String(Base64.encodeBase64(signData)), "UTF-8");
        if (token != null && token.equals(userToken)) {
            return true;
        }
        return false;
    }

}
