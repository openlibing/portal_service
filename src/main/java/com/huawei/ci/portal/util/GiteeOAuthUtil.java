package com.huawei.ci.portal.util;

import com.google.gson.reflect.TypeToken;
import com.huawei.ci.common.kms.KmsUtils;
import com.huawei.ci.common.utils.JsonUtils;
import com.netflix.config.DynamicPropertyFactory;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class GiteeOAuthUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(GiteeOAuthUtil.class);
    // 从配置中心获得
    private String CLIENT_ID = DynamicPropertyFactory.getInstance().getStringProperty("gitee.oauth.client.id","").get();
    private String CLIENT_SECRET = DynamicPropertyFactory.getInstance().getStringProperty("gitee.oauth.client.secret","").get();
    private String REDIRECT_URI = DynamicPropertyFactory.getInstance().getStringProperty("gitee.oauth.callback","").get();

    // 固定访问路径
    private static String AUTHORIZE_URL = "https://gitee.com/oauth/authorize" +
            "?client_id=%s&redirect_uri=%s&response_type=code";
    private static String TOKEN_URL = "https://gitee.com/oauth/token?grant_type=authorization_code&code=%s&client_id=%s&redirect_uri=%s&client_secret=%s";

    private static String INFO_URL = "https://gitee.com/api/v5/user?access_token=%s";

    private static GiteeOAuthUtil giteeOAuthUtil = new GiteeOAuthUtil();

    public static GiteeOAuthUtil getInstance(){
        return giteeOAuthUtil;
    }


    public String getLoginUrl() {
        return String.format(AUTHORIZE_URL, CLIENT_ID, REDIRECT_URI);
    }

    /**
     * 调用码云接口 获得access token
     * @param code 码云返回的识别码
     * @return
     */
    public HashMap<String, String> getToken(String code){
        HashMap<String, String> res = new HashMap<>();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/json;charset=UTF-8");
        HashMap<String, String> params = new HashMap<>();
        params.put("client_secret", CLIENT_SECRET);
        String content = JsonUtils.toJson(params);
        String format = String.format(TOKEN_URL, code, CLIENT_ID, REDIRECT_URI, CLIENT_SECRET);

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody data = RequestBody.create(mediaType, content);
        Request request = new Request.Builder()
                .url(format)
                .method("POST", data)
                .addHeader("Content-Type", "application/json")
                .build();
        try {
            Response response = client.newCall(request).execute();
            String result = response.body().string();
            res = JsonUtils.fromJson(result, new TypeToken<HashMap<String, String>>() {
            }.getType());

        } catch (IOException e) {
            LOGGER.error("get access_token from gitee error");
            res.put("error_message","get access_token from gitee error");
        }
        return res;
    }

    public static HashMap<String, String> getInfo(String accessToken) {
        HashMap<String, String> res = new HashMap<>();
        String url = String.format(INFO_URL, accessToken);

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(url)
                .method("GET", null)
                .build();
        try {
            Response response = client.newCall(request).execute();
            String result = response.body().string();
            res = JsonUtils.fromJson(result, new TypeToken<HashMap<String, String>>() {
            }.getType());
        } catch (IOException e) {
            LOGGER.error("get info from gitee error");
            res.put("error_message","get info from gitee error");
        }
        return res;
    }

}
