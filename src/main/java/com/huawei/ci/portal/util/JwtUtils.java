package com.huawei.ci.portal.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.huawei.ci.common.utils.JsonUtils;
import com.huawei.ci.portal.enums.GitEEConstants;
import com.huawei.ci.portal.enums.PermissionConstants;
import com.huawei.ci.portal.model.InfoModel;
import com.netflix.config.DynamicPropertyFactory;
import io.vertx.core.http.Cookie;
import io.vertx.ext.web.RoutingContext;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

public class JwtUtils {
    private static String JWT_SECRET = DynamicPropertyFactory.getInstance().getStringProperty("jwt.secret", "test_jwt_secret").get();

    public static String getJwtSecret() {
        return JwtUtils.JWT_SECRET;
    }

    public static String getToken(InfoModel infoModel) {
        Calendar expires = Calendar.getInstance();
        expires.add(Calendar.HOUR, 12);
        String token = "";
        token = JWT.create()
                .withAudience(infoModel.getAccessToken())
                .withExpiresAt(expires.getTime())
                .withClaim(PermissionConstants.ID, infoModel.getId())
                .withClaim(PermissionConstants.NAME, infoModel.getName())
                .withClaim(PermissionConstants.EMAIL, infoModel.getEmail())
                .withClaim(PermissionConstants.AVATAR_URL, infoModel.getAvatarUrl())
                .sign(Algorithm.HMAC256(JWT_SECRET));
        return token;
    }

    public static boolean verifyToken(String token, RoutingContext context) {
        DecodedJWT jwt = null;
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(JWT_SECRET)).build();
            verifier.verify(token);
            return true;
        } catch (Exception e) {
            //效验失败
            HashMap<String, String> res = new HashMap<>();
            res.put("msg", e.getMessage());
            res.put("result", "failed");
            HttpContextUtil.result(context, res, 400);
            return false;
        }
    }

    /**
     * 获取签发对象
     */
    public static String getAudience(String token) {
        String audience = null;
        try {
            audience = JWT.decode(token).getAudience().get(0);
        } catch (JWTDecodeException j) {

        }
        return audience;
    }


    /**
     * 通过载荷名字获取载荷的值
     */
    public static Claim getClaimByName(String token, String name) {
        return JWT.decode(token).getClaim(name);
    }

    public static Date getExpires(String token) {
        return JWT.decode(token).getExpiresAt();
    }

    public static String getTokenFromCookie(RoutingContext context) {
        Cookie tokenCookie = context.request().getCookie(PermissionConstants.TOKEN);
        if (Objects.isNull(tokenCookie)) {
            HashMap<String, Object> res = new HashMap<>();
            res.put("msg", "user do not login");
            res.put("result", new Object());
            String resultJson = JsonUtils.toJson(res);
            context.response().setStatusCode(200);
            context.response().setStatusMessage(GitEEConstants.SUCCESS);
            context.response().headers().add(GitEEConstants.CONTENT_LENGTH, String.valueOf(resultJson.length()));
            context.response().write(resultJson).close();
            return "";
        }
        return tokenCookie.getValue();
    }


}
