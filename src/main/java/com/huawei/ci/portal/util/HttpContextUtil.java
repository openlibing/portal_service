package com.huawei.ci.portal.util;

import com.huawei.ci.portal.enums.GitEEConstants;
import com.huawei.ci.portal.model.RestResponse;
import io.vertx.ext.web.RoutingContext;
import lombok.SneakyThrows;
import org.apache.servicecomb.foundation.common.utils.JsonUtils;

import java.util.HashMap;

public class HttpContextUtil {

    private static final int SUCCESS=200;
    private static final int REDIRECT_ERROR=302;
    private static final int TOKEN_ERROR=403;
    private static final int NO_PAGE=404;

    @SneakyThrows
    public static void ajaxRedirect(RoutingContext context, String url){
        RestResponse<String> restResponse=new RestResponse<>(REDIRECT_ERROR,"redirect",url);
        try {
            String responseStr= JsonUtils.writeValueAsString(restResponse);
            context.response().putHeader("Content-Type","text/plain");
            context.response().putHeader("Content-Length",String.valueOf(responseStr.length()));
            context.response().write(responseStr);
        }catch (Exception ignored){}

    }

    @SneakyThrows
    public static void response(RoutingContext context,int code){
        RestResponse<String> restResponse;
        if(code==403){
            restResponse=new RestResponse<>(TOKEN_ERROR,"no permit","no permit");
        }else if(code==404){
            restResponse=new RestResponse<>(NO_PAGE,"page not found","page not found");
        }else{
            restResponse=new RestResponse<>(SUCCESS,"success","success");
        }
        try {
            String responseStr= JsonUtils.writeValueAsString(restResponse);
            context.response().putHeader("Content-Type","text/plain");
            context.response().putHeader("Content-Length",String.valueOf(responseStr.length()));
            context.response().write(responseStr);
        }catch (Exception ignored){}

    }

    public static void result(RoutingContext context, HashMap<String, String> res, int code) {
        context.response().setStatusCode(code);
        String resJson = com.huawei.ci.common.utils.JsonUtils.toJson(res);
        context.response().headers().add(GitEEConstants.CONTENT_LENGTH, String.valueOf(resJson.length()));
        context.response().headers().add(GitEEConstants.CONTENT_TYPE, GitEEConstants.CONTENT_TYPE_VALUE);
        context.response().write(resJson).close();
        context.response().end();
    }
}
