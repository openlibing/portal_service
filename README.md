# CI门户服务

CI门户服务（ci-portal-service）与CI门户页面（ci-portal-website）组成CI门户网站，提供用户登录、CI数据查询、代码问题状态修改等功能。基于ServiceComb框架开发。
主要功能为：

- 集成码云OAuth登录
- 将请求转发给api-gateway服务
  - 基于URL前缀过滤，只将白名单中的URL转发给gateway
  - 在请求头中加入token、时间戳签名，由gateway校验请求是否合法
  - 将用户名、id放到query中，gateway将请求透传给backend服务，backend使用用户名、id对用户权限进行校验

## 原理机制

- 与gateway服务类似，扩展CommonHttpEdgeDispatcher类，当匹配/不匹配预定义的URL时，执行特定的操作。
- 与码云登录相关的URL在CI门户服务中实现相关逻辑(未在ci-portal-schemas中定义)：
  - /api/portal-service/login 判断用户是否登录，未登录则重定向至码云；已登录则不需要跳转
  - /api/portal-service/validateLogin 码云回调接口
  - /api/portal-service/logout 登录，注销CI门户Token和码云Token
  - /api/portal-service/getInfo 查询码云用户基本信息
- 在META-INF/services/org.apache.servicecomb.transport.rest.vertx.VertxHttpDispatcher文件中，增加Dispatcher类。
- 转发规则：src/main/resources/application.yml中servicecomb.http.dispatcher.edge.http
- 运行依赖
  - 配置中心: src/main/resources/application.yml中servicecomb.config.client
  - 注册中心: src/main/resources/application.yml中servicecomb.service.registry

## 代码说明

关键目录和文件：
│  .gitignore
│  pom.xml
│  README.md
│
└─src
   └─main
       ├─java
       │  └─com
       │      └─huawei
       │          └─ci
       │              └─portal
       │                  │  CiPortalServiceApplication.java # 启动类
       │                  │
       │                  ├─controller
       │                  │      AdminDispatcher.java
       │                  │      DefaultDispatcher.java
       │                  │      SelfDispatcher.java
       │                  │
       │                  ├─enums
       │                  │      GitEEConstants.java
       │                  │      PermissionConstants.java
       │                  │
       │                  ├─filter
       │                  │      AuthHandler.java
       │                  │
       │                  ├─model
       │                  │      InfoModel.java
       │                  │      RestResponse.java
       │                  │      TokenModel.java
       │                  │
       │                  └─util
       │                          AuthHelper.java
       │                          GiteeOAuthUtil.java
       │                          HttpContextUtil.java
       │                          JwtUtils.java
       │
       └─resources
           │  application-alpha.yml
           │  application-beta.yml
           │  application-local.yml
           │  application-prod.yml
           │  application.yml
           │  log4j2.xml
           │
           ├─config
           │      cse.handler.xml
           │
           └─META-INF
               └─services
                       org.apache.servicecomb.transport.rest.vertx.VertxHttpDispatcher
